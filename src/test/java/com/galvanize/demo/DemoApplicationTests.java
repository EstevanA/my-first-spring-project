package com.galvanize.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {


	@Test
	public void returnNumberShouldReturnOne() {
		DemoApplication demoApplication = new DemoApplication();
		int result = demoApplication.returnNumber();
		Assertions.assertEquals(1, result);
	}

	@Test
	void contextLoads() {
	}

}
